package com.example.service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.example.utils.Dictionary;

public class Validator {
	private Pattern dict1Regex;
	private Pattern dict2Regex;
	
	public Validator() {
		dict1Regex = Pattern.compile("[a-zA-Z]{4}");
		dict2Regex = Pattern.compile("[0-9]{5}");
	}
	
	boolean validation(String key, String dictName) throws IOException {
		try (BufferedReader fileReader = new BufferedReader(new FileReader(Dictionary.RESOURCES_PATH + dictName));
				Scanner scanner = new Scanner(fileReader)) {
			while(scanner.hasNext()) {
				String line = scanner.next();
				String[] words = line.split("-");
				if (key.equals(words[0]))return false;
			}
		}
		return dictName.equals(Dictionary.NAME_DICT_1) ? dict1Regex.matcher(key).matches() : dict2Regex.matcher(key).matches();
	}

}
