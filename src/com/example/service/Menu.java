package com.example.service;

import java.util.List;

import com.example.utils.Dictionary;

public class Menu {
	
	public static final int VIEW_CODE = 1;
	public static final int ADD_CODE = 2;
	public static final int DELETE_CODE = 3;
	public static final int SEARCH_CODE = 4;
	public static final int EXIT_CODE = 5;
	public static final int BACK_CODE = 6;
	
	public void printAction() {
		System.out.print("===-Menu-===\n" +
				VIEW_CODE + ") View dictionary;\n" + 
				ADD_CODE + ") Add word;\n" +
				DELETE_CODE + ") Delete word;\n" + 
				SEARCH_CODE + ") Translate word;\n" +
				EXIT_CODE + ") Close application;\n" + 
				"Type in item number and press Enter: ");		
	}

	public void printDictName(boolean forView) {
		System.out.print("===-Choose Dictionary-===\n" +
				Dictionary.NUMBER_DICT_1 + ") " + Dictionary.NAME_DICT_1 + ";\n" + 
				Dictionary.NUMBER_DICT_2 + ") " + Dictionary.NAME_DICT_2 + ";\n");
		if (forView) System.out.print(Dictionary.NUMBER_ALL_DICT + ") All dictionary;\n");
		System.out.print(BACK_CODE + ") Return to previous menu;\n" + 
				"Type in item number and press Enter: ");
	}
	
	public void printInputError(int action) {
		System.out.println("Item number " + action + " is not on the list.\n");
	}
	
	public void printDataEntryField() {
		System.out.print("Type in word and press Enter: ");
	}
	
	public void printWordTranslationField() {
		System.out.print("Type in word translation and press Enter: ");
	}
	
	public void printResultOfSearching(List<String> result) {
		System.out.println("Result of searching: ");
		result.stream().forEach(System.out::println);
		System.out.println();
	}
	
	public void printResultOfDeleting(String result) {
		System.out.println("Result of deleting: \n" + result);
	}
	
	public void printResultOfAdding(String result) {
		System.out.println("Result of adding: \n" + result);
	}

}
