package com.example.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.utils.Dictionary;

public class Editor {
	
	private static final String TEMP_FILE_NAME = "temp.txt";
	private Validator validator;
	
	public Editor() {
		validator = new Validator();
	}

	public void printDict(String ... dictNames)throws IOException {
		if (dictNames.length == 0) return;
		for (String dictName : dictNames) {
			if (dictName != null) {
				try (BufferedReader fileReader = new BufferedReader(new FileReader(Dictionary.RESOURCES_PATH + dictName));
						Scanner scanner = new Scanner(fileReader)) {
					System.out.print("\nDictionary " + dictName + ":\n");
					while(scanner.hasNextLine()) {
						System.out.println(scanner.nextLine());
					}
					System.out.println();
				} 
			}
		}
	}
	
	public String deleteByKey(String key, String dictName) throws IOException {
		boolean isDelete = false;
		String notFoundMsg = "Word " +  key + " in " + dictName + " not found.\n";
		String deletedWords = "";
		File file = new File(Dictionary.RESOURCES_PATH + TEMP_FILE_NAME);
		file.createNewFile();
		try (BufferedReader fileReader = new BufferedReader(new FileReader(Dictionary.RESOURCES_PATH + dictName));
				Scanner scanner = new Scanner(fileReader);
				BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file))) {
			while (scanner.hasNext()) {
				String line = scanner.next();
				String[] words = line.split(Dictionary.DELIMITER);
				if (!words[0].equals(key) || isDelete) {
					fileWriter.write(line + "\n");
				} else {
					isDelete = true;
					deletedWords += line + "\n";
				}
			}
		}
		if (isDelete) {
			File oldDict = new File(Dictionary.RESOURCES_PATH + dictName);
			oldDict.delete();
			file.renameTo(oldDict);
		}
		return isDelete ? deletedWords : notFoundMsg;
			
	}
	
	public String add(String key, String value, String dictName) throws IOException {
		StringBuilder sb = new StringBuilder();
		if(validator.validation(key, dictName)) {
			try (BufferedWriter fileWriter = new BufferedWriter(new FileWriter(Dictionary.RESOURCES_PATH + dictName, true))) {
				sb.append(key).append(Dictionary.DELIMITER).append(value).append("\n");
				fileWriter.append(sb.toString());
			}
			return sb.toString();
		} else {
			return "Words(keys) must be unique.\n"
					+ "Words in dict1.txt must match the condition [a-z A-Z]{4}\n"
					+ "Words in dict2.txt must match the condition [0-9]{5}\n";
		}
		
	}
	
	public List<String> searchByKey(String key, String dictName) throws IOException {
		List<String> result = new ArrayList<>();
		try (BufferedReader fileReader = new BufferedReader(new FileReader(Dictionary.RESOURCES_PATH + dictName));
				Scanner scanner = new Scanner(fileReader)) {
			while(scanner.hasNext()) {
				String line = scanner.next();
				String[] words = line.split("-");
				if (key.equals(words[0])) result.add(line);
			}
		}
		return result;
	}
}
