package com.example.utils;

public final class Dictionary {
	public static final String DELIMITER = "-";
	public static final String NAME_DICT_1 = "dict_1.txt";
	public static final String NAME_DICT_2 = "dict_2.txt";
	public static final String RESOURCES_PATH = "./resources//";
	public static final int NUMBER_DICT_1 = 1;
	public static final int NUMBER_DICT_2 = 2;
	public static final int NUMBER_ALL_DICT = 3;
	public static final int COUNT_DICTS = 2;
}
