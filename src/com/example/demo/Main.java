package com.example.demo;

import java.io.IOException;
import java.util.Scanner;

import com.example.service.Editor;
import com.example.service.Menu;
import com.example.utils.Dictionary;

public class Main {
	public static void main (String[] args) {
		int action;
		Menu menu = new Menu();
		Editor editor = new Editor();
		try (Scanner scanner = new Scanner(System.in)) {
			do {
				menu.printAction();
				action = scanner.nextInt();
				System.out.println();
				switch (action) {
					case Menu.VIEW_CODE -> {
						do {
							menu.printDictName(true);
							action = scanner.nextInt();
							System.out.println();
							if (action == Menu.BACK_CODE) break;
							if (action == Dictionary.NUMBER_DICT_1) {
								editor.printDict(Dictionary.NAME_DICT_1);
							} else if (action == Dictionary.NUMBER_DICT_2) {
								editor.printDict(Dictionary.NAME_DICT_2);
							} else if (action == Dictionary.NUMBER_ALL_DICT) {
								editor.printDict(Dictionary.NAME_DICT_1, Dictionary.NAME_DICT_2);
							} else {
								menu.printInputError(action);
							}
						} while (action != 6);
						break;
					}
					case Menu.ADD_CODE -> {
						do {
							menu.printDictName(false);
							action = scanner.nextInt();
							System.out.println();
							if (action == Menu.BACK_CODE) break;
							menu.printDataEntryField();
							String key = scanner.next();
							System.out.println();
							menu.printWordTranslationField();
							scanner.nextLine();
							String value = scanner.nextLine();
							System.out.println();
							if (action == Dictionary.NUMBER_DICT_1) {
								menu.printResultOfAdding(editor.add(key, value, Dictionary.NAME_DICT_1));
							} else if (action == Dictionary.NUMBER_DICT_2) {
								menu.printResultOfAdding(editor.add(key, value, Dictionary.NAME_DICT_2));
							} else {
								menu.printInputError(action);
							}
						} while (action != Menu.BACK_CODE);
						break;
					}
					case Menu.DELETE_CODE -> {
						do {
							menu.printDictName(false);
							action = scanner.nextInt();
							System.out.println();
							if (action == Menu.BACK_CODE) break;
							menu.printDataEntryField();
							String key = scanner.next();
							System.out.println();
							if (action == Dictionary.NUMBER_DICT_1) {
								menu.printResultOfDeleting(editor.deleteByKey(key, Dictionary.NAME_DICT_1));
							} else if (action == Dictionary.NUMBER_DICT_2) {
								menu.printResultOfDeleting(editor.deleteByKey(key, Dictionary.NAME_DICT_2));
							} else {
								menu.printInputError(action);
							}
						} while (action != Menu.BACK_CODE);
						break;
					}
					case Menu.SEARCH_CODE -> {
						do {
							menu.printDictName(false);
							action = scanner.nextInt();
							System.out.println();
							if (action == Menu.BACK_CODE) break;
							menu.printDataEntryField();
							String key = scanner.next();
							System.out.println();
							if (action == Dictionary.NUMBER_DICT_1) {
								menu.printResultOfSearching(editor.searchByKey(key, Dictionary.NAME_DICT_1));
							} else if (action == Dictionary.NUMBER_DICT_2) {
								menu.printResultOfSearching(editor.searchByKey(key, Dictionary.NAME_DICT_2));
							} else {
								menu.printInputError(action);
							}
						} while (action != Menu.BACK_CODE);
						break;
					}
					case Menu.EXIT_CODE -> {break;}
					default -> {
						menu.printInputError(action);
					}
				}
			} while(action != Menu.EXIT_CODE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
